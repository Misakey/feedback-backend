package controller

import (
	"net/http"

	"github.com/labstack/echo"
	"github.com/xtgo/uuid"
	"gitlab.com/Misakey/msk-sdk-go/merror"
)

type FeedbackEcho struct {
	service serviceFeedback
}

// FeedbackEcho constructor
func NewFeedbackEcho(service serviceFeedback) *FeedbackEcho {
	return &FeedbackEcho{
		service: service,
	}
}

// SoftDelete
func (fe *FeedbackEcho) SoftDelete(ctx echo.Context) error {
	id := ctx.Param("id")

	_, err := uuid.Parse(id)
	if err != nil {
		return merror.BadRequest().Detail("id", merror.DVMalformed).Describe(err.Error()).From(merror.OriQuery)
	}

	if err := fe.service.SoftDelete(ctx.Request().Context(), id); err != nil {
		return merror.Transform(err).Describe("could not delete user's feedbacks")
	}
	return ctx.NoContent(http.StatusNoContent)
}
