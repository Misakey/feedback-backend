package controller

import (
	"context"

	"gitlab.com/Misakey/msk-sdk-go/patch"
	
	"gitlab.com/Misakey/feedback-backend/src/model"
)

type serviceRating interface {
	CreateRating(ctx context.Context, rating *model.Rating) error
	ListRating(ctx context.Context, filters model.RatingFilters) ([]*model.Rating, error)
	PartialUpdateRating(ctx context.Context, ratingPI *patch.Info) error
	HardDelete(ctx context.Context, id int) error
}

type serviceFeedback interface {
	SoftDelete(ctx context.Context, userID string) error
}

type serviceAppContribution interface {
	Create(ctx context.Context, contribution *model.ApplicationContribution) error
	List(ctx context.Context) ([]*model.ApplicationContribution, error)
	Delete(ctx context.Context, id int) error
}
