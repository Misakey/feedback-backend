package controller

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/feedback-backend/src/model"
)

type appContributionEcho struct {
	service serviceAppContribution
}

func NewAppContributionEcho(service serviceAppContribution) *appContributionEcho {
	return &appContributionEcho{
		service: service,
	}
}

// Create is the /application-contributions POST handler
func (r *appContributionEcho) Create(ctx echo.Context) error {
	contribution := model.ApplicationContribution{}

	if err := ctx.Bind(&contribution); err != nil {
		return merror.BadRequest().From(merror.OriBody).Describe(err.Error())
	}

	if err := ctx.Validate(&contribution); err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	if err := r.service.Create(ctx.Request().Context(), &contribution); err != nil {
		return merror.Transform(err).Describe("could not create contribution")
	}

	return ctx.JSON(http.StatusCreated, contribution)
}

// List is the /application-contributions GET handler
func (r *appContributionEcho) List(ctx echo.Context) error {
	contributions, err := r.service.List(ctx.Request().Context())
	if err != nil {
		return merror.Transform(err).Describe("could not find contribution")
	}

	return ctx.JSON(http.StatusOK, contributions)
}

// Delete is the /application/contributions DELETE handler
func (r *appContributionEcho) Delete(ctx echo.Context) error {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		return merror.BadRequest().From(merror.OriPath).Describe(err.Error()).Detail("id", merror.DVMalformed)
	}

	if err := r.service.Delete(ctx.Request().Context(), id); err != nil {
		return merror.Transform(err).Describe("could not delete application contribution")
	}
	return ctx.NoContent(http.StatusNoContent)
}
