package controller

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo"
	"gitlab.com/Misakey/msk-sdk-go/merror"
	"gitlab.com/Misakey/msk-sdk-go/patch"

	"gitlab.com/Misakey/feedback-backend/src/adaptor/param"
	"gitlab.com/Misakey/feedback-backend/src/model"
)

type RatingEcho struct {
	service serviceRating
}

// RatingEcho constructor
func NewRatingEcho(service serviceRating) *RatingEcho {
	return &RatingEcho{
		service: service,
	}
}

// Create
func (r *RatingEcho) Create(ctx echo.Context) error {
	rating := model.Rating{}

	if err := ctx.Bind(&rating); err != nil {
		return merror.BadRequest().From(merror.OriBody).Describe(err.Error())
	}

	if err := ctx.Validate(&rating); err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	if err := r.service.CreateRating(ctx.Request().Context(), &rating); err != nil {
		return merror.Transform(err).Describe("could not create rating")
	}
	return ctx.JSON(http.StatusCreated, rating)
}

// Find
func (r *RatingEcho) Find(ctx echo.Context) error {
	var err error

	// handle filters parameters
	filters := model.RatingFilters{}
	filters.ApplicationID = ctx.QueryParam("application_id")
	filters.UserID = ctx.QueryParam("user_id")
	filters.WithUsers, err = param.ToOptionalBool(ctx.QueryParam("with_users"))
	if err != nil {
		return merror.BadRequest().From(merror.OriQuery).Detail("with_users", merror.DVInvalid)
	}

	if err := ctx.Validate(&filters); err != nil {
		return merror.Transform(err).From(merror.OriQuery)
	}

	ratings, err := r.service.ListRating(ctx.Request().Context(), filters)
	if err != nil {
		return merror.Transform(err).Describe("could not find rating")
	}

	return ctx.JSON(http.StatusOK, ratings)
}

// PartialUpdate
func (r *RatingEcho) PartialUpdate(ctx echo.Context) error {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		return merror.BadRequest().From(merror.OriPath).Describe(err.Error()).Detail("id", merror.DVMalformed)
	}

	// bind patched data to our model
	patchMap := patch.FromHTTP(ctx.Request()).ToModel(model.Rating{})
	if patchMap.Invalid {
		return merror.BadRequest().From(merror.OriBody).Describe("invalid patch map")
	}

	rating := model.Rating{}
	rating.ID = id
	patchInfo := patchMap.GetInfo(&rating)
	err = ctx.Bind(&rating)
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Describe(err.Error())
	}

	// Validate
	err = ctx.Validate(&patchInfo)
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Describe(err.Error())
	}

	err = r.service.PartialUpdateRating(ctx.Request().Context(), &patchInfo)
	if err != nil {
		return merror.Transform(err).Describe("could not patch rating")
	}
	return ctx.NoContent(http.StatusNoContent)

}

// HardDelete
func (r *RatingEcho) HardDelete(ctx echo.Context) error {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		return merror.BadRequest().From(merror.OriPath).Describe(err.Error()).Detail("id", merror.DVMalformed)
	}

	if err := r.service.HardDelete(ctx.Request().Context(), id); err != nil {
		return merror.Transform(err).Describe("could not delete rating")
	}
	return ctx.NoContent(http.StatusNoContent)
}
