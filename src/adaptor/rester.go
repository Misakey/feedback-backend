package adaptor

import (
	"context"
	"net/url"
)

// Rester represents an interface for REST clients.
type Rester interface {
	Get(ctx context.Context, route string, params url.Values, output interface{}) error
	Post(ctx context.Context, route string, params url.Values, input interface{}, output interface{}) error
	Put(ctx context.Context, route string, params url.Values, input interface{}, output interface{}) error
	Delete(ctx context.Context, route string, params url.Values) error
}
