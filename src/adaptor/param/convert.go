package param

import "strconv"

func ToOptionalBool(param string) (*bool, error) {
	if len(param) == 0 {
		return nil, nil
	}
	b, err := strconv.ParseBool(param)
	if err != nil {
		return nil, err
	}
	return &b, nil
}
