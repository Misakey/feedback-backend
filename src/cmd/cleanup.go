package cmd

import (
	"context"
	"os"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/Misakey/msk-sdk-go/config"
	"gitlab.com/Misakey/msk-sdk-go/logger"

	"gitlab.com/Misakey/feedback-backend/src/adaptor/sql"
	"gitlab.com/Misakey/feedback-backend/src/repo"
	"gitlab.com/Misakey/feedback-backend/src/service"
)

var cleanupCmd = &cobra.Command{
	Use:   "cleanup",
	Short: "Cleanup some resources",
	Long: `Hard delete resources marked as to_delete when a specific number of days
	has passed since the soft deletion. This number of days is defined in the cleanup section
	of the configuration file.`,
	Run: func(cmd *cobra.Command, args []string) {
		runCleanup()
	},
}

func init() {
	RootCmd.AddCommand(cleanupCmd)
}

func runCleanup() {
	// init logger
	zerologger := logger.ZerologLogger()
	log.Logger = zerologger
	ctx := context.Background()
	ctx = context.WithValue(ctx, "logger", &zerologger)

	viper.AddConfigPath("/etc/")

	// handle missing mandatory fields
	mandatoryFields := []string{
		"cleanup.time_before_deletion",
	}

	// handle envs
	switch env {
	case "production":
		viper.SetConfigName("feedback-config")
	case "development":
		viper.SetConfigName("feedback-config.dev")
		log.Info().Msg("{} Development mode is activated. {}")
	default:
		log.Fatal().Msg("unknown ENV value (should be production|development)")
	}

	// try reading in a config
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal().Err(err).Msg("could not read configuration")
	}
	config.FatalIfMissing(mandatoryFields)

	// init db connections
	dbConn, err := sql.NewPostgreConn(os.Getenv("APP_FEEDBACK_DB_URL"))
	if err != nil {
		log.Fatal().Err(err).Msg("could not connect to db")
	}

	ratingRepo := repo.NewRatingPSQL(dbConn)

	cleanupManager := service.NewCleanupManager(
		ratingRepo,
		viper.GetDuration("cleanup.time_before_deletion"),
	)

	err = cleanupManager.CleanupRatings(ctx)
	if err != nil {
		log.Fatal().Err(err).Msg("cleanup ratings failed")
	}
	log.Info().Msg("cleanup job successfull")
}
