package cmd

import (
	"fmt"
	"os"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/Misakey/msk-sdk-go/bubble"
	"gitlab.com/Misakey/msk-sdk-go/config"
	mecho "gitlab.com/Misakey/msk-sdk-go/echo"
	"gitlab.com/Misakey/msk-sdk-go/logger"
	mhttp "gitlab.com/Misakey/msk-sdk-go/rester/http"

	"gitlab.com/Misakey/feedback-backend/src/adaptor/sql"
	"gitlab.com/Misakey/feedback-backend/src/controller"
	"gitlab.com/Misakey/feedback-backend/src/repo"
	"gitlab.com/Misakey/feedback-backend/src/service"
)

var cfgFile string
var goose string
var env = os.Getenv("ENV")

func init() {
	cobra.OnInitialize()
	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file")
	RootCmd.PersistentFlags().StringVar(&goose, "goose", "up", "goose command")
	RootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

var RootCmd = &cobra.Command{
	Use:   "feedback",
	Short: "Run the application feedback service",
	Long:  `This service handles user feedback about applications: ratings, comments...`,
	Run: func(cmd *cobra.Command, args []string) {
		initService()
	},
}

func initService() {
	// init logger
	log.Logger = logger.ZerologLogger()

	initDefaultConfig()

	// add error needles to auto handle some specific errors on layers we use everywhere
	bubble.AddNeedle(bubble.PSQLNeedle{})
	bubble.AddNeedle(bubble.ValidatorNeedle{})
	bubble.AddNeedle(bubble.EchoNeedle{})
	bubble.Lock()

	e := echo.New()

	// init echo framework with compressed HTTP responses, custom logger format and custom validator
	e.Use(mecho.NewZerologLogger())
	e.Use(mecho.NewLogger())
	e.Use(mecho.NewCORS())
	e.Use(middleware.Recover())

	e.Validator = mecho.NewValidator()
	e.HideBanner = true
	e.HTTPErrorHandler = mecho.Error

	// init db connection
	var dbConn, err = sql.NewPostgreConn(os.Getenv("APP_FEEDBACK_DB_URL"))
	if err != nil {
		log.Fatal().Err(err).Msg("db connection failed")
	}

	// init auth middleware
	jwtValidator := mecho.NewJWTMidlw(true)
	jwtModerateValidator := mecho.NewJWTMidlw(false)

	// create HTTP Resters
	authRester := mhttp.NewClient(viper.GetString("auth.endpoint"), false)
	applicationRester := mhttp.NewClient(viper.GetString("application.endpoint"), false)

	// init repositories
	ratingRepo := repo.NewRatingPSQL(dbConn)
	userRepo := repo.NewUserHTTP(authRester, viper.GetString("server.token"))
	applicationRepo := repo.NewApplicationHTTP(applicationRester)
	appContributionRepo := repo.NewAppContributionPSQL(dbConn)

	// init services
	feedbackManager := service.NewFeedbackManager(ratingRepo, userRepo, applicationRepo, appContributionRepo)

	// init controllers
	feedbackController := controller.NewFeedbackEcho(feedbackManager)
	ratingController := controller.NewRatingEcho(feedbackManager)
	appContributionController := controller.NewAppContributionEcho(feedbackManager)

	// Init generic
	genericController := controller.NewGeneric()

	// bind routes
	rating := e.Group("ratings")
	rating.POST("", ratingController.Create, jwtValidator)
	rating.GET("", ratingController.Find, jwtModerateValidator)
	rating.PATCH("/:id", ratingController.PartialUpdate, jwtValidator)
	rating.DELETE("/:id", ratingController.HardDelete, jwtValidator)

	user := e.Group("users")
	user.DELETE("/:id", feedbackController.SoftDelete, jwtValidator)

	contribution := e.Group("application-contributions")
	contribution.POST("", appContributionController.Create, jwtValidator)
	contribution.GET("", appContributionController.List, jwtValidator)
	contribution.DELETE("/:id", appContributionController.Delete, jwtValidator)

	generic := e.Group("")
	generic.GET("/version", genericController.Version)

	// launch echo server
	e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", viper.GetInt("server.port"))))
}

func initDefaultConfig() {
	// always look for the configuration file in the /etc folder
	viper.AddConfigPath("/etc/")

	// set defaults for configuration
	viper.SetDefault("server.port", 5000)
	viper.SetDefault("hydra.secure", true)

	// handle missing mandatory fields
	mandatoryFields := []string{
		"server.token",
		"server.cors",
		"hydra.admin_url",
		"auth.endpoint",
	}

	secretFields := []string{
		"server.token",
	}

	// handle envs
	switch env {
	case "production":
		viper.SetConfigName("feedback-config")
	case "development":
		viper.SetConfigName("feedback-config.dev")
		log.Info().Msg("{} Development mode is activated. {}")
	default:
		log.Fatal().Msg("unknown ENV value (should be production|development)")
	}

	// try reading in a config
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal().Err(err).Msg("could not read configuration")
	}
	config.FatalIfMissing(mandatoryFields)
	config.Print(secretFields)
}
