package model

import (
	"time"

	"github.com/volatiletech/null"
)

type RatingFilters struct {
	UserID        string `validate:"omitempty,uuid"`
	ApplicationID string `validate:"omitempty,uuid"`
	ToDelete      null.Bool
	UpdatedBefore time.Time
	WithUsers     *bool
}
