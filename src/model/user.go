package model

type User struct {
	ID          string `json:"id,omitempty"`
	DisplayName string `json:"display_name"`
	AvatarURI   string `json:"avatar_uri"`
}
