package repo

import (
	"context"
	"fmt"

	"gitlab.com/Misakey/msk-sdk-go/rester"

	"gitlab.com/Misakey/feedback-backend/src/model"
)

// ApplicationHTTP used for applications info from application api
type ApplicationHTTP struct {
	application rester.Client
}

// ApplicationHTTP constructor
func NewApplicationHTTP(application rester.Client) *ApplicationHTTP {
	return &ApplicationHTTP{
		application: application,
	}
}

// Get
func (ah *ApplicationHTTP) Get(ctx context.Context, id string) (*model.Application, error) {
	var application *model.Application
	route := fmt.Sprintf("/application-info/%s", id)

	err := ah.application.Get(ctx, route, nil, &application)

	return application, err
}
