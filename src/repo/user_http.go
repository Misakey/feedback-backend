package repo

import (
	"context"
	"net/url"
	"strings"

	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/rester"

	"gitlab.com/Misakey/feedback-backend/src/model"
)

type UserHTTP struct {
	auth         rester.Client
	serviceToken string
}

func NewUserHTTP(auth rester.Client, serviceToken string) *UserHTTP {
	return &UserHTTP{
		auth:         auth,
		serviceToken: serviceToken,
	}
}

// ListUsers
func (uh *UserHTTP) List(ctx context.Context, ids []string) ([]*model.User, error) {
	var users []*model.User

	// set Service JWT in context to forward it
	ctx = ajwt.SetAccessClaimsJWT(ctx, uh.serviceToken)

	params := url.Values{}
	if len(ids) != 0 {
		params.Add("ids", strings.Join(ids, ","))
	}

	err := uh.auth.Get(ctx, "/users", params, &users)
	return users, err
}
