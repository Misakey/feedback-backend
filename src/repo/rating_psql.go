package repo

import (
	"context"
	"database/sql"

	"github.com/lib/pq"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/feedback-backend/src/adaptor/slices"
	"gitlab.com/Misakey/feedback-backend/src/model"
)

// RatingPSQL used for user's rating storage using sqlboiler ORM
type RatingPSQL struct {
	db *sql.DB
}

// RatingPSQL constructor
func NewRatingPSQL(db *sql.DB) *RatingPSQL {
	return &RatingPSQL{
		db: db,
	}
}

// Insert
func (r *RatingPSQL) Insert(ctx context.Context, rating *model.Rating) error {
	err := rating.Insert(ctx, r.db, boil.Infer())
	if err != nil {
		// try to consider error cause as pq error to understand deeper the error
		pqErr, ok := merror.Cause(err).(*pq.Error)
		if ok && pqErr.Code.Name() == "unique_violation" && pqErr.Constraint == "rating_user_id_application_id_key" {
			return merror.Conflict().Describe(err.Error()).
				Detail("user_id", merror.DVConflict).
				Detail("application_id", merror.DVConflict)
		}
	}
	return err
}

// Get
func (r *RatingPSQL) Get(ctx context.Context, id int) (*model.Rating, error) {
	rating, err := model.FindRating(ctx, r.db, id)
	if err == sql.ErrNoRows {
		return nil, merror.NotFound().Describe(err.Error()).Detail("id", merror.DVNotFound)
	}
	return rating, err
}

// List
func (r *RatingPSQL) List(ctx context.Context, filters model.RatingFilters) ([]*model.Rating, error) {
	mods := []qm.QueryMod{}

	if filters.ApplicationID != "" {
		mods = append(mods, qm.Where(`application_id = ?`, filters.ApplicationID))
	}

	if filters.UserID != "" {
		mods = append(mods, qm.Where(`user_id = ?`, filters.UserID))
	}

	if !filters.ToDelete.IsZero() {
		mods = append(mods, qm.Where(`to_delete = ?`, filters.ToDelete))
	}

	if !filters.UpdatedBefore.IsZero() {
		mods = append(mods, qm.Where(`updated_at < ?`, filters.UpdatedBefore))
	}

	ratings, err := model.Ratings(mods...).All(ctx, r.db)

	// Return an empty array if no result
	if ratings == nil {
		ratings = []*model.Rating{}
	}
	return ratings, err
}

// PartialUpdate
func (r *RatingPSQL) PartialUpdate(ctx context.Context, rating *model.Rating, fields ...string) error {
	// always add updated_at field (should not be added by the service)
	fields = append(fields, "updated_at")
	rowsAff, err := rating.Update(ctx, r.db, boil.Whitelist(fields...))
	if err != nil {
		return err
	}
	if rowsAff == 0 {
		return merror.NotFound().Describe("no rows affected")
	}
	return nil
}

// HardDelete: delete the resource
func (r *RatingPSQL) HardDelete(ctx context.Context, rating *model.Rating) error {
	rowsAff, err := rating.Delete(ctx, r.db)
	if err != nil {
		return err
	}
	if rowsAff == 0 {
		return merror.NotFound().Describe("no rows affected")
	}
	return nil
}

// BulkSoftDelete takes a list of user IDs and soft delete them
func (r *RatingPSQL) BulkSoftDelete(ctx context.Context, userID string) error {
	rowsAff, err := model.Ratings(qm.Where(`user_id = ?`, userID)).
		UpdateAll(ctx, r.db, model.M{"to_delete": true})
	if err != nil {
		return err
	}
	if rowsAff == 0 {
		return merror.NotFound().Describe("no rows affected")
	}
	return nil
}

// BulkHardDelete
func (r *RatingPSQL) BulkHardDelete(ctx context.Context, IDs []int) error {
	rowsAff, err := model.Ratings(qm.WhereIn(`id in ?`, slices.IntSliceToInterfaceSlice(IDs)...)).DeleteAll(ctx, r.db)
	if err != nil {
		return err
	}
	if rowsAff == 0 {
		return merror.NotFound().Describe("no rows affected")
	}
	return nil
}
