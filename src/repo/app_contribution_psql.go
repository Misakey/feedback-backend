package repo

import (
	"context"
	"database/sql"

	"github.com/lib/pq"
	"github.com/volatiletech/sqlboiler/boil"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/feedback-backend/src/model"
)

// AppContributionPSQL used for user's contribution storage using sqlboiler ORM
type AppContributionPSQL struct {
	db *sql.DB
}

// AppContributionPSQL constructor
func NewAppContributionPSQL(db *sql.DB) *AppContributionPSQL {
	return &AppContributionPSQL{
		db: db,
	}
}

// Insert
func (acp *AppContributionPSQL) Insert(ctx context.Context, contribution *model.ApplicationContribution) error {
	err := contribution.Insert(ctx, acp.db, boil.Infer())
	if err != nil {
		// try to consider error cause as pq error to understand deeper the error
		pqErr, ok := merror.Cause(err).(*pq.Error)
		if ok && pqErr.Code.Name() == "unique_violation" && pqErr.Constraint == "application_contribution_pkey" {
			return merror.Conflict().Describe(err.Error()).
				Detail("id", merror.DVConflict)
		}
	}
	return err
}

// List
func (acp *AppContributionPSQL) List(ctx context.Context) ([]*model.ApplicationContribution, error) {
	return model.ApplicationContributions().All(ctx, acp.db)
}

// Delete
func (acp *AppContributionPSQL) Delete(ctx context.Context, id int) error {
	rowsAff, err := model.ApplicationContributions(model.ApplicationContributionWhere.ID.EQ(id)).DeleteAll(ctx, acp.db)
	if rowsAff == 0 {
		return merror.NotFound().Describe(err.Error()).Detail("id", merror.DVNotFound)
	}

	return err
}
