package service

import (
	"context"

	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"
	"gitlab.com/Misakey/msk-sdk-go/patch"

	"gitlab.com/Misakey/feedback-backend/src/model"
)

// Create
func (fm *FeedbackManager) CreateRating(ctx context.Context, rating *model.Rating) error {
	// grab accesses from context
	acc := ajwt.GetAccesses(ctx)
	if acc == nil {
		return merror.Forbidden()
	}

	// only user can post comment
	if acc.IsNotUser(rating.UserID) {
		return merror.Forbidden()
	}

	err := fm.ratings.Insert(ctx, rating)
	if err != nil {
		return err
	}
	return nil
}

// List
func (fm *FeedbackManager) ListRating(ctx context.Context, filters model.RatingFilters) ([]*model.Rating, error) {
	filters.ToDelete.SetValid(false)
	ratings, err := fm.ratings.List(ctx, filters)
	if err != nil {
		return nil, err
	}

	// user IDs list to get authors
	userIDs := []string{}

	// return directly ratings if no need to add users
	if filters.WithUsers == nil || !*filters.WithUsers {
		// remove user ids from ratings before returning it
		for _, rating := range ratings {
			rating.UserID = ""
		}
		return ratings, nil
	}

	// the call must bear a valid token to retrieve authors of ratings
	acc := ajwt.GetAccesses(ctx)
	if acc == nil {
		return nil, merror.Forbidden()
	}

	// build list of user ids to list
	for _, rating := range ratings {
		userIDs = append(userIDs, rating.UserID)
	}

	// retrieve authors
	authors, err := fm.users.List(ctx, userIDs)
	if err != nil {
		return nil, err
	}

	// build a map of uuid:users to get efficiently author later
	authorMap := make(map[string]*model.User, len(authors))
	for _, author := range authors {
		authorMap[author.ID] = author
	}

	// bind authors to their ratings
	for i, _ := range ratings {
		author, ok := authorMap[ratings[i].UserID]
		if ok {
			ratings[i].User = author
			// we empty the ID since we don't want to return it
			ratings[i].User.ID = ""
			ratings[i].UserID = ""
		}
	}
	return ratings, nil
}

// PartialUpdate :
func (fm *FeedbackManager) PartialUpdateRating(ctx context.Context, ratingPI *patch.Info) error {
	patchedRating := ratingPI.Model.(*model.Rating)

	// get rating and verify it exists
	rating, err := fm.ratings.Get(ctx, patchedRating.ID)
	if err != nil {
		return err
	}

	// grab accesses from context
	acc := ajwt.GetAccesses(ctx)

	// verify requested user id and authenticated user id are the same.
	if acc == nil || acc.IsNotUser(rating.UserID) {
		return merror.Forbidden()
	}

	// filter fields we want to patch
	ratingPI.Whitelist([]string{"Comment", "Value"})

	return fm.ratings.PartialUpdate(ctx, patchedRating, ratingPI.Output...)
}

// HardDelete
func (fm *FeedbackManager) HardDelete(ctx context.Context, id int) error {
	// get rating and verify it exists
	rating, err := fm.ratings.Get(ctx, id)
	if err != nil {
		return err
	}

	// grab accesses from context
	acc := ajwt.GetAccesses(ctx)

	// verify requested user id and authenticated user id are the same.
	if acc == nil || acc.IsNotUser(rating.UserID) {
		return merror.Forbidden()
	}

	return fm.ratings.HardDelete(ctx, rating)
}
