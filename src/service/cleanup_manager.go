package service

import (
	"time"
)

type CleanupManager struct {
	ratings ratingRepo

	// time before definitely deleting resources
	timeBeforeDeletion time.Duration
}

// CleanupManager constructor
func NewCleanupManager(
	ratings ratingRepo,
	timeBeforeDeletion time.Duration,
) *CleanupManager {
	return &CleanupManager{
		ratings:            ratings,
		timeBeforeDeletion: timeBeforeDeletion,
	}
}
