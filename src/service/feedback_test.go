package service

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/feedback-backend/src/model"
)

func TestSoftDelete(t *testing.T) {

	tests := map[string]struct {
		acc                  *ajwt.AccessClaims
		rating               *model.Rating
		ratingGet            func(context.Context, int) (*model.Rating, error)
		ratingList           func(context.Context, model.RatingFilters) ([]*model.Rating, error)
		ratingBulkSoftDelete func(ctx context.Context, userID string) error
		userID               string
		expectedErr          error
	}{
		"a call without jwt is forbiden": {
			userID: "uuid_1",
			ratingGet: func(_ context.Context, id int) (*model.Rating, error) {
				assert.Equal(t, 1, id, "rating ID rating partial update")
				return &model.Rating{
					ID:            1,
					UserID:        "uuid_1",
					ApplicationID: "uuid_2",
				}, nil
			},
			expectedErr: merror.Forbidden(),
		},
		"a call without scope user is forbiden": {
			userID: "uuid_1",
			acc: &ajwt.AccessClaims{
				Scope:   "",
				Subject: "uuid_1",
			},
			ratingGet: func(_ context.Context, id int) (*model.Rating, error) {
				assert.Equal(t, 1, id, "rating ID rating partial update")
				return &model.Rating{
					ID:            1,
					UserID:        "uuid_1",
					ApplicationID: "uuid_2",
				}, nil
			},
			expectedErr: merror.Forbidden(),
		},
		"a call with scope misadmin is forbiden": {
			userID: "uuid_1",
			acc: &ajwt.AccessClaims{
				Scope:   "misadmin",
				Subject: "uuid_1",
			},
			ratingGet: func(_ context.Context, id int) (*model.Rating, error) {
				assert.Equal(t, 1, id, "rating ID rating partial update")
				return &model.Rating{
					ID:            1,
					UserID:        "uuid_1",
					ApplicationID: "uuid_2",
				}, nil
			},
			expectedErr: merror.Forbidden(),
		},
		"a call with scope user, role admin and not owner of the rating can't delete the rating": {
			userID: "uuid_1",
			acc: &ajwt.AccessClaims{
				Scope:   "user rol.admin.uuid_2",
				Subject: "uuid_0",
			},
			ratingGet: func(_ context.Context, id int) (*model.Rating, error) {
				assert.Equal(t, 1, id, "rating ID rating partial update")
				return &model.Rating{
					ID:            1,
					UserID:        "uuid_1",
					ApplicationID: "uuid_2",
				}, nil
			},
			expectedErr: merror.Forbidden(),
		},
		"a call with scope user, role admin and owner of the rating can't delete the rating": {
			userID: "uuid_1",
			acc: &ajwt.AccessClaims{
				Scope:   "user rol.admin.uuid_2",
				Subject: "uuid_1",
			},
			ratingGet: func(_ context.Context, id int) (*model.Rating, error) {
				assert.Equal(t, 1, id, "rating ID rating partial update")
				return &model.Rating{
					ID:            1,
					UserID:        "uuid_1",
					ApplicationID: "uuid_2",
				}, nil
			},
			ratingList: func(_ context.Context, filters model.RatingFilters) ([]*model.Rating, error) {
				assert.Equal(t, "uuid_1", filters.UserID, "rating softDelete id")
				return []*model.Rating{&model.Rating{
					ID:            1,
					UserID:        "uuid_1",
					ApplicationID: "uuid_2"},
				}, nil
			},
			ratingBulkSoftDelete: func(_ context.Context, userID string) error {
				assert.Equal(t, "uuid_1", userID, "rating softDelete id")
				return nil
			},
			expectedErr: merror.Forbidden(),
		},
		"a call with scope user and user is owner of the rating can't delete his rating": {
			userID: "uuid_1",
			acc: &ajwt.AccessClaims{
				Scope:   "user",
				Subject: "uuid_1",
			},
			ratingList: func(_ context.Context, filters model.RatingFilters) ([]*model.Rating, error) {
				assert.Equal(t, "uuid_1", filters.UserID, "rating softDelete id")
				return []*model.Rating{&model.Rating{
					ID:            1,
					UserID:        "uuid_1",
					ApplicationID: "uuid_2"},
				}, nil
			},
			ratingBulkSoftDelete: func(ctx context.Context, userID string) error {
				assert.Equal(t, "uuid_1", userID, "rating softDelete id")
				return nil
			},
			expectedErr: merror.Forbidden(),
		},
		"a call with scope service can delete ratings": {
			userID: "uuid_1",
			acc: &ajwt.AccessClaims{
				Scope:   "service",
				Subject: "uuid_1",
			},
			ratingList: func(_ context.Context, filters model.RatingFilters) ([]*model.Rating, error) {
				assert.Equal(t, "uuid_1", filters.UserID, "rating softDelete id")
				return []*model.Rating{&model.Rating{
					ID:            1,
					UserID:        "uuid_1",
					ApplicationID: "uuid_2"},
				}, nil
			},
			ratingBulkSoftDelete: func(ctx context.Context, userID string) error {
				assert.Equal(t, "uuid_1", userID, "rating softDelete id")
				return nil
			},
			expectedErr: nil,
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := FeedbackManager{
				ratings: &ratingsMock{
					bulkSoftDelete: test.ratingBulkSoftDelete,
					get:            test.ratingGet,
					list:           test.ratingList,
				},
			}

			// call function to test
			err := service.SoftDelete(ctx, test.userID)

			// check assertions
			assert.Equal(t, test.expectedErr, err)
		})

	}
}
