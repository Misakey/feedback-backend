package service

import (
	"context"
	"time"

	"gitlab.com/Misakey/msk-sdk-go/logger"

	"gitlab.com/Misakey/feedback-backend/src/model"
)

func (cm *CleanupManager) CleanupRatings(ctx context.Context) error {
	logger.FromCtx(ctx).Info().Msg("starting cleanup job")

	// get all ratings that has been soft deleted
	// for more than timeBeforeDeletion
	softDeleteBefore := time.Now().Add(-cm.timeBeforeDeletion)

	// hard delete ratings
	deleteFilters := model.RatingFilters{}
	deleteFilters.UpdatedBefore = softDeleteBefore
	deleteFilters.ToDelete.SetValid(true)
	ratingsToHardDelete, err := cm.ratings.List(ctx, deleteFilters)
	if err != nil {
		return err
	}

	if ratingsToHardDelete == nil || len(ratingsToHardDelete) == 0 {
		logger.FromCtx(ctx).Info().
			Msg("no rating to hard delete")
	} else {
		ratingIDs := make([]int, len(ratingsToHardDelete))
		for index, rating := range ratingsToHardDelete {
			logger.FromCtx(ctx).Info().
				Msgf("planning rating with id=%d deletion", rating.ID)
			ratingIDs[index] = rating.ID
		}
		if err := cm.ratings.BulkHardDelete(ctx, ratingIDs); err != nil {
			return err
		}
	}
	return nil
}
