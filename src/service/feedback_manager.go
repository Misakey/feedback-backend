package service

type FeedbackManager struct {
	ratings       ratingRepo
	applications  applicationRepo
	users         userRepo
	contributions appContributionRepo
}

// FeedbackManager constructor
func NewFeedbackManager(ratings ratingRepo, users userRepo, applications applicationRepo, contributions appContributionRepo) *FeedbackManager {
	return &FeedbackManager{
		ratings:       ratings,
		users:         users,
		applications:  applications,
		contributions: contributions,
	}
}
