package service

import (
	"context"

	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/feedback-backend/src/model"
)

// SoftDelete
// set to_delete on true for all ratings owned by the user
func (rm *FeedbackManager) SoftDelete(ctx context.Context, userID string) error {
	filters := model.RatingFilters{}

	acc := ajwt.GetAccesses(ctx)
	if acc == nil {
		return merror.Forbidden()
	}

	// service can delete any userID's ratings
	if acc.IsNotAnyService() {
		return merror.Forbidden()
	}

	filters.UserID = userID

	if err := rm.ratings.BulkSoftDelete(ctx, userID); err != nil &&
		!merror.HasCode(err, merror.NotFoundCode) {
		return err
	}

	return nil
}
