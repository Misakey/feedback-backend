package service

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"
	"gitlab.com/Misakey/msk-sdk-go/patch"

	"gitlab.com/Misakey/feedback-backend/src/model"
)

type ratingsMock struct {
	insert         func(context.Context, *model.Rating) error
	get            func(context.Context, int) (*model.Rating, error)
	list           func(context.Context, model.RatingFilters) ([]*model.Rating, error)
	partialUpdate  func(context.Context, *model.Rating, ...string) error
	hardDelete     func(context.Context, *model.Rating) error
	bulkSoftDelete func(ctx context.Context, userID string) error
}

func (m ratingsMock) Insert(ctx context.Context, r *model.Rating) error {
	return m.insert(ctx, r)
}

func (m ratingsMock) Get(ctx context.Context, id int) (*model.Rating, error) {
	return m.get(ctx, id)
}

func (m ratingsMock) List(ctx context.Context, f model.RatingFilters) ([]*model.Rating, error) {
	return m.list(ctx, f)
}
func (m ratingsMock) PartialUpdate(ctx context.Context, rating *model.Rating, fields ...string) error {
	return m.partialUpdate(ctx, rating)
}

func (m ratingsMock) BulkSoftDelete(ctx context.Context, userID string) error {
	return m.bulkSoftDelete(ctx, userID)
}

func (m ratingsMock) HardDelete(ctx context.Context, r *model.Rating) error {
	return m.hardDelete(ctx, r)
}

func (m ratingsMock) BulkHardDelete(ctx context.Context, IDs []int) error {
	return nil
}

type usersMock struct {
	list func(context.Context, []string) ([]*model.User, error)
}

func (m usersMock) List(ctx context.Context, ids []string) ([]*model.User, error) {
	return m.list(ctx, ids)
}

func TestList(t *testing.T) {
	withoutUsers := false
	withUsers := true

	tests := map[string]struct {
		acc             *ajwt.AccessClaims
		filters         model.RatingFilters
		ratingList      func(context.Context, model.RatingFilters) ([]*model.Rating, error)
		userList        func(context.Context, []string) ([]*model.User, error)
		expectedRatings []*model.Rating
		expectedErr     error
	}{
		"a call without jwt and with_users=false retrieves ratings without users & user ids": {
			acc:     nil,
			filters: model.RatingFilters{WithUsers: &withoutUsers},
			ratingList: func(context.Context, model.RatingFilters) ([]*model.Rating, error) {
				return []*model.Rating{
					&model.Rating{ID: 1, UserID: "uuid_1"},
					&model.Rating{ID: 2, UserID: "uuid_3"},
				}, nil
			},
			expectedRatings: []*model.Rating{
				&model.Rating{ID: 1, UserID: ""},
				&model.Rating{ID: 2, UserID: ""},
			},
		},
		"a call without jwt and with_users=true return a forbidden": {
			acc:     nil,
			filters: model.RatingFilters{WithUsers: &withUsers},
			ratingList: func(context.Context, model.RatingFilters) ([]*model.Rating, error) {
				return []*model.Rating{
					&model.Rating{ID: 1, UserID: "uuid_1"},
					&model.Rating{ID: 2, UserID: "uuid_3"},
				}, nil
			},
			expectedErr: merror.Forbidden(),
		},
		"a call with a jwt and with_users=true returns ratings with authors": {
			acc:     &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			filters: model.RatingFilters{WithUsers: &withUsers},
			ratingList: func(context.Context, model.RatingFilters) ([]*model.Rating, error) {
				return []*model.Rating{
					&model.Rating{ID: 1, UserID: "uuid_1"},
					&model.Rating{ID: 2, UserID: "uuid_3"},
				}, nil
			},
			userList: func(_ context.Context, ids []string) ([]*model.User, error) {
				assert.Equal(t, []string{"uuid_1", "uuid_3"}, ids, "users list")
				return []*model.User{
					&model.User{ID: "uuid_1", DisplayName: "Michel", AvatarURI: "http://michel-picture.fr"},
					&model.User{ID: "uuid_3", DisplayName: "Micheline", AvatarURI: "http://micheline-picture.fr"},
				}, nil
			},
			expectedRatings: []*model.Rating{
				&model.Rating{ID: 1, UserID: "", User: &model.User{DisplayName: "Michel", AvatarURI: "http://michel-picture.fr"}},
				&model.Rating{ID: 2, UserID: "", User: &model.User{DisplayName: "Micheline", AvatarURI: "http://micheline-picture.fr"}},
			},
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := FeedbackManager{
				ratings: &ratingsMock{list: test.ratingList},
				users:   &usersMock{list: test.userList},
			}

			// call function to test
			ratings, err := service.ListRating(ctx, test.filters)

			// check assertions
			assert.Equal(t, test.expectedRatings, ratings)
			assert.Equal(t, test.expectedErr, err)
		})

	}
}

func TestPartialUpdateRating(t *testing.T) {

	tests := map[string]struct {
		acc                 *ajwt.AccessClaims
		ratingPI            *patch.Info
		ratingGet           func(context.Context, int) (*model.Rating, error)
		ratingPartialUpdate func(ctx context.Context, rating *model.Rating, fields ...string) error
		expectedErr         error
	}{
		"a call without jwt is forbiden": {
			ratingPI: &patch.Info{
				Input:  []string{"Comment"},
				Output: []string{"Comment"},
				Model:  &model.Rating{ID: 1},
			},
			ratingGet: func(_ context.Context, id int) (*model.Rating, error) {
				assert.Equal(t, 1, id, "rating ID rating partial update")
				return &model.Rating{
					ID:            1,
					UserID:        "uuid_1",
					ApplicationID: "uuid_2",
				}, nil
			},
			expectedErr: merror.Forbidden(),
		},
		"a call without scope user is forbiden": {
			acc: &ajwt.AccessClaims{
				Scope:   "",
				Subject: "uuid_0",
			},
			ratingPI: &patch.Info{
				Input:  []string{"Comment"},
				Output: []string{"Comment"},
				Model:  &model.Rating{ID: 1},
			},
			ratingGet: func(_ context.Context, id int) (*model.Rating, error) {
				assert.Equal(t, 1, id, "rating ID rating partial update")
				return &model.Rating{
					ID:            1,
					UserID:        "uuid_1",
					ApplicationID: "uuid_2",
				}, nil
			},
			expectedErr: merror.Forbidden(),
		},
		"a call with scope misdamin is forbiden": {
			acc: &ajwt.AccessClaims{
				Scope:   "misadmin",
				Subject: "uuid_0",
			},
			ratingPI: &patch.Info{
				Input:  []string{"Comment"},
				Output: []string{"Comment"},
				Model:  &model.Rating{ID: 1},
			},
			ratingGet: func(_ context.Context, id int) (*model.Rating, error) {
				assert.Equal(t, 1, id, "rating ID rating partial update")
				return &model.Rating{
					ID:            1,
					UserID:        "uuid_1",
					ApplicationID: "uuid_2",
				}, nil
			},
			expectedErr: merror.Forbidden(),
		},
		"a call without scope user id and role admin of the application can't modify the rating": {
			acc: &ajwt.AccessClaims{
				Scope:   "user rol.admin.uuid_2",
				Subject: "uuid_0",
			},
			ratingPI: &patch.Info{
				Input:  []string{"Comment"},
				Output: []string{"Comment"},
				Model:  &model.Rating{ID: 1},
			},
			ratingGet: func(_ context.Context, id int) (*model.Rating, error) {
				assert.Equal(t, 1, id, "rating ID rating partial update")
				return &model.Rating{
					ID:            1,
					UserID:        "uuid_1",
					ApplicationID: "uuid_2",
				}, nil
			},
			expectedErr: merror.Forbidden(),
		},
		"a call with scope user and user is owner of the rating can modify his rating": {
			acc: &ajwt.AccessClaims{
				Scope:   "user",
				Subject: "uuid_1",
			},
			ratingPI: &patch.Info{
				Input:  []string{"Comment"},
				Output: []string{"Comment"},
				Model:  &model.Rating{ID: 1, ApplicationID: "uuid_2"},
			},
			ratingGet: func(_ context.Context, id int) (*model.Rating, error) {
				assert.Equal(t, 1, id, "rating ID rating partial update")
				return &model.Rating{
					ID:            1,
					UserID:        "uuid_1",
					ApplicationID: "uuid_2",
				}, nil
			},
			ratingPartialUpdate: func(_ context.Context, rating *model.Rating, _ ...string) error {
				assert.Equal(t, 1, rating.ID, "rating ID rating partial update")
				assert.Equal(t, "uuid_2", rating.ApplicationID, "applicationID rating partial update")
				return nil
			},
			expectedErr: nil,
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := FeedbackManager{
				ratings: &ratingsMock{
					get:           test.ratingGet,
					partialUpdate: test.ratingPartialUpdate,
				},
			}

			// call function to test
			err := service.PartialUpdateRating(ctx, test.ratingPI)

			// check assertions
			assert.Equal(t, test.expectedErr, err)
		})

	}
}

func TestHardDelete(t *testing.T) {

	tests := map[string]struct {
		acc              *ajwt.AccessClaims
		rating           *model.Rating
		ratingGet        func(context.Context, int) (*model.Rating, error)
		ratingHardDelete func(ctx context.Context, rating *model.Rating) error
		ratingID         int
		expectedErr      error
	}{
		"a call without jwt is forbiden": {
			ratingID: 1,
			ratingGet: func(_ context.Context, id int) (*model.Rating, error) {
				assert.Equal(t, 1, id, "rating ID rating partial update")
				return &model.Rating{
					ID:            1,
					UserID:        "uuid_1",
					ApplicationID: "uuid_2",
				}, nil
			},
			expectedErr: merror.Forbidden(),
		},
		"a call without scope user is forbiden": {
			ratingID: 1,
			acc: &ajwt.AccessClaims{
				Scope:   "",
				Subject: "uuid_1",
			},
			ratingGet: func(_ context.Context, id int) (*model.Rating, error) {
				assert.Equal(t, 1, id, "rating ID rating partial update")
				return &model.Rating{
					ID:            1,
					UserID:        "uuid_1",
					ApplicationID: "uuid_2",
				}, nil
			},
			expectedErr: merror.Forbidden(),
		},
		"a call with scope misadmin is forbiden": {
			ratingID: 1,
			acc: &ajwt.AccessClaims{
				Scope:   "misadmin",
				Subject: "uuid_1",
			},
			ratingGet: func(_ context.Context, id int) (*model.Rating, error) {
				assert.Equal(t, 1, id, "rating ID rating partial update")
				return &model.Rating{
					ID:            1,
					UserID:        "uuid_1",
					ApplicationID: "uuid_2",
				}, nil
			},
			expectedErr: merror.Forbidden(),
		},
		"a call with scope user, role admin and not owner of the rating can't delete the rating": {
			ratingID: 1,
			acc: &ajwt.AccessClaims{
				Scope:   "user rol.admin.uuid_2",
				Subject: "uuid_0",
			},
			ratingGet: func(_ context.Context, id int) (*model.Rating, error) {
				assert.Equal(t, 1, id, "rating ID rating partial update")
				return &model.Rating{
					ID:            1,
					UserID:        "uuid_1",
					ApplicationID: "uuid_2",
				}, nil
			},
			expectedErr: merror.Forbidden(),
		},
		"a call with scope user, role admin and owner of the rating can delete the rating": {
			ratingID: 1,
			acc: &ajwt.AccessClaims{
				Scope:   "user rol.admin.uuid_2",
				Subject: "uuid_1",
			},
			ratingGet: func(_ context.Context, id int) (*model.Rating, error) {
				assert.Equal(t, 1, id, "rating ID rating partial update")
				return &model.Rating{
					ID:            1,
					UserID:        "uuid_1",
					ApplicationID: "uuid_2",
				}, nil
			},
			ratingHardDelete: func(ctx context.Context, rating *model.Rating) error {
				assert.Equal(t, 1, rating.ID, "rating hardDelete id")
				return nil
			},
			expectedErr: nil,
		},
		"a call with scope user and user is owner of the rating can delete his rating": {
			ratingID: 1,
			acc: &ajwt.AccessClaims{
				Scope:   "user",
				Subject: "uuid_1",
			},
			ratingGet: func(_ context.Context, id int) (*model.Rating, error) {
				assert.Equal(t, 1, id, "rating ID rating partial update")
				return &model.Rating{
					ID:            1,
					UserID:        "uuid_1",
					ApplicationID: "uuid_2",
				}, nil
			},
			ratingHardDelete: func(ctx context.Context, rating *model.Rating) error {
				assert.Equal(t, 1, rating.ID, "rating hardDelete id")
				return nil
			},
			expectedErr: nil,
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := FeedbackManager{
				ratings: &ratingsMock{
					hardDelete: test.ratingHardDelete,
					get:        test.ratingGet,
				},
			}

			// call function to test
			err := service.HardDelete(ctx, test.ratingID)

			// check assertions
			assert.Equal(t, test.expectedErr, err)
		})

	}
}
