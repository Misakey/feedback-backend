package service

import (
	"context"

	"gitlab.com/Misakey/feedback-backend/src/model"
)

type ratingRepo interface {
	Insert(ctx context.Context, user *model.Rating) error
	Get(ctx context.Context, id int) (*model.Rating, error)
	List(ctx context.Context, filters model.RatingFilters) ([]*model.Rating, error)
	PartialUpdate(ctx context.Context, rating *model.Rating, fields ...string) error
	HardDelete(ctx context.Context, rating *model.Rating) error
	BulkSoftDelete(ctx context.Context, userID string) error
	BulkHardDelete(ctx context.Context, IDs []int) error
}

type userRepo interface {
	List(ctx context.Context, ids []string) ([]*model.User, error)
}

type applicationRepo interface {
	Get(ctx context.Context, id string) (*model.Application, error)
}

type appContributionRepo interface {
	Insert(ctx context.Context, user *model.ApplicationContribution) error
	List(ctx context.Context) ([]*model.ApplicationContribution, error)
	Delete(ctx context.Context, id int) error
}
