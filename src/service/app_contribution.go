package service

import (
	"context"

	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/feedback-backend/src/model"
)

// Create
func (r *FeedbackManager) Create(ctx context.Context, contribution *model.ApplicationContribution) error {
	// grab accesses from context
	acc := ajwt.GetAccesses(ctx)
	if acc == nil {
		return merror.Forbidden()
	}

	// only user can post a contribution
	if acc.IsNotUser(contribution.UserID) {
		return merror.Forbidden()
	}

	// check if application exists
	_, err := r.applications.Get(ctx, contribution.ApplicationID)
	if err != nil {
		return err
	}

	return r.contributions.Insert(ctx, contribution)
}

// List
func (r *FeedbackManager) List(ctx context.Context) ([]*model.ApplicationContribution, error) {
	// grab accesses from context
	acc := ajwt.GetAccesses(ctx)
	if acc == nil {
		return nil, merror.Forbidden()
	}

	// only misadmin can list contributions
	if acc.IsNotMisadmin() {
		return nil, merror.Forbidden()
	}

	return r.contributions.List(ctx)
}

// Delete
func (r *FeedbackManager) Delete(ctx context.Context, id int) error {
	// grab accesses from context
	acc := ajwt.GetAccesses(ctx)
	if acc == nil {
		return merror.Forbidden()
	}

	// only misadmin can delete a contribution
	if acc.IsNotMisadmin() {
		return merror.Forbidden()
	}

	return r.contributions.Delete(ctx, id)
}
