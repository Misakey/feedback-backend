package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190517162938, Down20190517162938)
}

func Up20190517162938(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE TABLE rating(
    id SERIAL PRIMARY KEY,
    user_id UUID NOT NULL,
    application_id UUID NOT NULL,
    value SMALLINT NOT NULL,
    created_at TIMESTAMP NOT NULL,
    UNIQUE (user_id, application_id)
	);`)
	return err
}

func Down20190517162938(tx *sql.Tx) error {
	_, err := tx.Exec(`DROP TABLE rating;`)
	return err
}
