package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190704143227, Down20190704143227)
}

func Up20190704143227(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE rating
     ADD COLUMN updated_at timestamptz NOT NULL,
     ADD COLUMN to_delete BOOL NOT NULL DEFAULT false;
	`)
	return err
}

func Down20190704143227(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE rating
    DROP COLUMN updated_at,
		DROP COLUMN to_delete;
	`)
	return err
}
