package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190805110318, Down20190805110318)
}

func Up20190805110318(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE rating
     ADD COLUMN comment TEXT NOT NULL;
	`)
	return err
}

func Down20190805110318(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE rating
    DROP COLUMN comment;
	`)
	return err
}
