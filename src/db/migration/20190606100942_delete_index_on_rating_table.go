package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190606100942, Down20190606100942)
}

// Drop this index : postgre already index uuid
// And we cannot index as UNIQUE cuz application_id is added at each new rating on same app
func Up20190606100942(tx *sql.Tx) error {
	_, err := tx.Exec(`DROP INDEX application_idx;`)
	return err
}

func Down20190606100942(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE UNIQUE INDEX application_idx ON rating (application_id);`)
	return err
}
