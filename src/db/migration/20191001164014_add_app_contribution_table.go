package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(upAddAppContributionTable, downAddAppContributionTable)
}

func upAddAppContributionTable(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE TABLE application_contribution(
    id SERIAL PRIMARY KEY,
    user_id UUID NOT NULL,
    application_id UUID NOT NULL,
    dpo_email VARCHAR (255),
    link VARCHAR (255)
    );`)
	return err
}

func downAddAppContributionTable(tx *sql.Tx) error {
	_, err := tx.Exec(`DROP TABLE application_contribution;`)
	return err
}
