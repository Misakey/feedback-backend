package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190522102541, Down20190522102541)
}

func Up20190522102541(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE UNIQUE INDEX application_idx ON rating (application_id);`)
	return err
}

func Down20190522102541(tx *sql.Tx) error {
	_, err := tx.Exec(`DROP INDEX application_idx;`)
	return err
}
