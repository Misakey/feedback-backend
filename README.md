# Introduction
feedback-backend is responsible for managing users' feedback on application.

## Table of content

* [Introduction](#introduction)
* [Folder Architecture](#folder-architecture)
* [Run and Develop](#run-and-develop)
  * [Dependencies](#dependencies)
    * [Docker](#docker)
    * [SQLBoiler](#sqlboiler)
  * [Configuration](#configuration)
    * [Environment](#environment)
    * [Configuration File](#configuration-file)
  * [Database Migrations](#database-migrations)
    * [Migrate Your Database](#migrate-your-database)
    * [Create Migrations](#create-migrations)
  * [Database populate](#database-populate)
* [Cleanup Job](#cleanup-job)
* [Deployment](#deployment)

------

## Folder architecture

_Main folders:_

- `src`: code sources.
- `docs`: swagger and postman files.
- `config`: feedback config example and sqlboiler configuration.

_Concerning the source code:_

Feedback is meant to manage feedback from users about applications: rating, reports…

At the source level, each modules and future services aims to follow Clean Architecture principles ([here is a quick introduction](http://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)) and Layered Architecture.
Here is the list of folders and a description:
- `cmd`: cobra commands
- `controller`: presenters layers, handling data transformation (usually from JSON to internal model)
- `service`: most of the business logic, interact with repo to play with models/data
- `repository`: handle retrieval/storage of models (can use a DB, an API...)
- `model`: our model, understandable and used by all our layers
- `adaptor`: code linked to frameworks and drivers we use to build our service, mostly for customisation purpose

------

## Run and Develop

### Dependencies
##### Docker

The project is using [docker](https://www.docker.com/community-edition) to facilitate the run and the deployment.

For both service and migration builds, the default Dockerfile get dependencies over internet.

If you want to use already downloaded dependencies (local), you can use the `Dockerfile.dev` which uses local vendor folder to build binary.

### Configuration

Configuration is done on **2 levels**:
- [some mandatory environment variables](#environment) (used mostly for credentials)
- [a configuration file](#configuration-file) (we use [viper](https://github.com/spf13/viper#what-is-viper) so many possibilities in term of format)

##### Environment

Environment variables are needed for some internal libraries. We also store secret in environment today.

Here is the list of variable to set up to have the account service working properly:

- `FEEDBACK_DB_URL`: the [postgres URL](https://jdbc.postgresql.org/documentation/80/connect.html) to connect to the feedback database.
- `ENV`: the environment mode you choose (`production` or `environment`):
  - Use a different [configuration file](#configuration-file).

##### Configuration File

The service always tries to read the config file from the `/etc` directly.

The configuration file name must be `feedback` with the extension corresponding to chosen format.
In development mode, it tries to read `feedback-config.dev.{extension}` (extension being one managed by [viper](https://github.com/spf13/viper#what-is-viper)).
This file is automatically added to the built Docker image using `config/feedback-config.toml`.

An example a of configuration file (using TOML) is available in `config` folder.
In production mode, it tries to read `feedback-config.{extension}` (extension being one managed by [viper](https://github.com/spf13/viper#what-is-viper)).
This file is **NOT** automatically added to the built Docker image. A volume must be mounted to share it.

An example a of configuration file (using TOML) is available in the `config` folder.


### Database Migrations

Database Migrations are handled by [Pressly Goose](https://github.com/pressly/goose) which is a fork improving [Liamstask Goose](https://bitbucket.org/liamstask/goose).
The migration tool is encapsulated in a Docker container.


##### Migrate Your Database

To migrate the database you just have to launch the `feedback_backend` container which run automatically the `feedback_migrate` and the `feedback_db`.

The default command sent to goose is `up` which apply all missing migrations to your running database.

As an example, if you use docker-compose to run your containers, you can override this command by setting [docker-compose command](https://docs.docker.com/compose/compose-file/compose-file-v2/#command).

The following syntax would perform a `goose down` instead of the default `goose up`.

```yaml
  feedback_migrate:
    command:
      "migrate --goose=down"
```

##### Create Migrations

See [goose usage](https://github.com/pressly/goose#usage) to create migrations.

You must use `goose` locally to create migrations, since the migration file must created on your local storage and not inside a container, it is more practical.
So basically do not use Docker to perform this action :).

A hint of what could look your migration creation command line:

`goose -dir=./src/db/migration/ postgres "postgres://misakey:secret@localhost:5432/feedback?sslmode=disable" create add_rating_table`

#### Models generation with SQLBoiler

If you need to alter the SQL database scheme, you'll need to install [sqlboiler](https://github.com/volatiletech/sqlboiler#download) which is the ORM we use.

SQLBoiler is a scheme based ORM which generates models according to SQL scheme in order to optimize queries.

:warning: As SQLBoiler builds its models by reading a database, you need to have a running database with all migrations **properly** performed. You can just run `docker-compose up --build` in the `src/db` folder to run such a database.

You can then regenerate models using the following command, more information [here](https://github.com/volatiletech/sqlboiler#initial-generation):

`sqlboiler psql --wipe --config config/sqlboiler.toml --pkgname model`

SQLBoiler generates by default a `models` folder, so you must move generated files and replace existing one with the new versions.
:warning: We advise to not use `--output=src/model` option since model files unrelated to sqlboiler would be removed.
:warning: Be careful, all existing files will be overriden inside this directory, so be aware of struct tags extension that have been put in place on top of sqlboiler models for example !

## Cleanup Job

The `feedback-backend` binary includes a cleanup job.

You can use it by running test-and-run and launching:

```
docker run --network test-and-run_misakey_vpn -e "ENV=development" -e "APP_FEEDBACK_DB_URL=postgres://misakey:secret@feedback_db:5432/application-feedback?sslmode=disable" registry.gitlab.com/misakey/feedback-backend:<YOUR TAG> cleanup 
```
It is intended to be used as a cronjob and takes the same configuration file as the main command.

Its specific configuration lies in the `[cleanup]` section.

It also needs the `ENV` and the `DATABASE_URL` environment variables.

It will scan the db and hard deletes objects that need to be deleted.

## Deployment

:warning: Make sure the corresponding images exist before running the following commands.

Create a `config.yaml` file with a `config:` key and the `config.toml` production content.

When you want to deploy the application for the first time, clone the repo, checkout to the desired tag, then run:

```
helm install --name feedback-backend helm/feedback-backend -f path/to/config.yaml --set env=production --set ddenv=preprod --set image.tag=latest

```

If you just need to upgrade the application, then run:
```
helm upgrade feedback-backend helm/feedback-backend -f path/to/config.yaml --set env=production --set ddenv=preprod --set image.tag=latest
```

The command will hang during the migration. Check in another terminal if there is no problem with the migration.

If the migration fails, CTRL+C your helm command, then run `kubectl delete jobs feedback-backend-migrate`, then solve the problem and re-run the helm command.
