module gitlab.com/Misakey/feedback-backend

go 1.12

require (
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.2.9 // indirect
	github.com/lib/pq v1.2.0
	github.com/pkg/errors v0.8.1
	github.com/pressly/goose v2.7.0-rc3+incompatible
	github.com/rs/zerolog v1.15.0
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.4.0
	github.com/stretchr/testify v1.4.0
	github.com/volatiletech/inflect v0.0.0-20170731032912-e7201282ae8d // indirect
	github.com/volatiletech/null v8.0.0+incompatible
	github.com/volatiletech/sqlboiler v3.2.0+incompatible
	github.com/xtgo/uuid v0.0.0-20140804021211-a0b114877d4c
	gitlab.com/Misakey/msk-sdk-go v0.0.0-20191025072618-05128997d0fb
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7 // indirect
	golang.org/x/sys v0.0.0-20190626221950-04f50cda93cb // indirect
	golang.org/x/text v0.3.2 // indirect
)
