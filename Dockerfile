FROM golang:1.12 as builder

WORKDIR /go/src/app
COPY . .
RUN GO111MODULE=on CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build \
    -mod=vendor \
    -installsuffix 'static' \
    -o /bin/feedback-backend .

ENTRYPOINT ["/bin/feedback-backend"]

FROM alpine:3.9

RUN until apk --update add ca-certificates; do sleep 1; done
COPY --from=builder /bin/feedback-backend /bin/feedback-backend
# add configuration file as configuration dev file to be integrated directly in our images
# in production, the file must be served via a mounted volume
COPY ./config/feedback-config.toml /etc/feedback-config.dev.toml

# set the version env variable used in the /version endpoint
ARG VERSION=unset
ENV VERSION $VERSION

EXPOSE 5000

ENTRYPOINT ["/bin/feedback-backend"]
