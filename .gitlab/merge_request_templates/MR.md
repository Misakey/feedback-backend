## Summary/TL;DR
> Write a summary of the merge request. Describe contexts, the why of general changes...


---
## Close issues
> * List all issues to close when MR is merged.
> * To trigger auto close, link issues preceded by "Closes". See [docs](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)


---
## Screenshots|Schemas|Code samples
> Paste json schemas/examples when related to API or Data model


---
## :boom: :rotating_light: Breaking changes
> Remove block and title if no breaking change


---
## :exclamation: Tips
> Do not forget to add labels

> Remove all blockquotes when template is filled

/wip
> Wip status signals that this MR is not ready for review yet. Remove wip status only after all conditions are passed

> When MR is created, `Cut/Paste` "Removing wip status" section from description and insert a comment with it.

> Use `// @FIXME` as a comment over code you didn't have time to optimize/improve and think it should be tracked and changed later

> When reviewing, Copy/Paste "Review checklist" and insert a comment describing your review

> When reviewing, if prerequisites are not all OK, just postpone your review and ping back the assignee

---
## Removing wip status
* [ ] I created a comment containing this section
* [ ] I filled the description template, then removed blockquotes
* [ ] I read each change file by file, line by line :imp:
* [ ] Commits follow rules
* [ ] Source branch is up-to-date compared to target branch
* [ ] Environment is running
* [ ] Pipeline was successful
* [ ] I considered refactoring code to avoid duplication
* [ ] I added unit tests
* [ ] I updated api docs
* [ ] I documented breaking changes :boom: :rotating_light:

---
## Review checklist

```
* Prerequisites
  * [ ] No rebase needed at the time of my review
  * [ ] Pipeline is successful at the time of my review
* Functional
  * [ ] I ran the project, MR scope is working
  * [ ] Functional requirements are followed
* Architecture quality
  * [ ] Formalisms/rules are respected
  * [ ] No dead code added
  * Code coherence
    * [ ] Clear, clean and readable code
    * [ ] Separation of concerns
    * [ ] Code behaviour is self-explanatory or has comments to explain
  * [ ] No refactor needed
* [ ] `// @FIXME` code reviewed and changes are planned (if applicable)
```
